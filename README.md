# simple-forward-tracking

Simple stand-alone forward tracking algorithm to perform MC based studies.
--------------------------------------------------------------------------

## HOW TO: simple-forward-tracking
- python tracking.py execute -h *# to get help on the execute mode*


- python tracking.py preprocess -r [RUN(s)] -e [EVENT(s)] *# see available runs and events in the Data section*


- *and then:* python tracking.py execute -r [RUN(s)] -e [EVENT(s)] *# use only events that has been preprocessed*


- *or, simply:* python tracking.py execute -p -r [RUN(s)] -e [EVENT(s)] *# any available runs / events. Pre-procession step will be done automatically.*

## Examples
- python tracking.py -h
- python tracking.py display
- python tracking.py display -s
- python tracking.py execute -p -r 2505
- python tracking.py preprocess -r 2500 2501 -e 1 2 3 4 5
- python tracking.py execute -r 2500 2501 -e 1 2 3 4 5

### Parameters
- required
    - mode (python tracking.py MODE):
        - display
        - execute
        - preprocess

- optional
    - [display] run (-r [RUN]) a run number
    - [display] event (-e [EVENT]) an event number
    - [display] save (-s) use this flag to save 'event display' figures into the 'output/'' folder
    - [display] verbosity (-v [VERBOSITY]) verbosity level number



    - [execute] run (-r [RUN1] [RUN2] ...) one or several run numbers
    - [execute] event (-e [EVENT1] [EVENT2] ...) one or several event numbers
    - [execute] preprocess (-p) use this flag to do pre-procession automatically
    - [execute] verbosity (-v [VERBOSITY]) verbosity level number



    - [preprocess] run (-r [RUN1] [RUN2] ...) one or several run numbers
    - [preprocess] event (-e [EVENT1] [EVENT2] ...) one or several event numbers

## Data
https://cernbox.cern.ch/index.php/s/tm7NBVgwQBcnFOb
- Available runs: 2500, 2501, 2503, 2504, 2505
- Available events : [1 - 50]

## Requirements
- python (>= 3.7 recommended)
- ROOT
- numpy
- uproot3
- argparse
- matplotlib
- scipy

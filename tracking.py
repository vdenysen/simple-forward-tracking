from service.service import *
from service.smearing import *
from service.fit import *

def preproc_ntuple(_run_number = None, _event_number = None):
    """
    Preprocession of the mcparticle_data/{run}_MCtracks.root file.
    mcparticle_data/{run}_FThits.root will be created.

    Args:
        _run_number     = [int], run number of the data file you want to use: [2500 - 2599]
        _event_number   = [int], event number of the data: [1 - 50]
    """

    if _run_number is None:
        _run_number = np.array([2500, 2501, 2503, 2504, 2505])
    if _event_number is None:
        _event_number = np.arange(1, 51, 1, dtype = int)

    if not isinstance(_run_number, np.ndarray) and not isinstance(_run_number, list):
        _run_number = np.array([_run_number])

    if not isinstance(_event_number, np.ndarray) and not isinstance(_event_number, list):
        _event_number = np.array([_event_number])

    for run_number in _run_number:
        for event_number in _event_number:
            tree = up.open("mcparticle_data/" + str(run_number) + "_MCtracks.root")["MCParticleNTuple/hits_itself"]
            #                               0    1    2    3    4     5      6
            hits = np.asarray(tree.arrays(["x", "y", "z", "p", "q", "pid","part"], outputtype=tuple)).T
            event = tree.arrays(["event"], outputtype=tuple)[0]

            event_index = np.where(event == event_number)[0]
            hits = hits[event_index]

            unique_particle = {}
            for hit in hits:
                if hit[6] not in unique_particle:
                    unique_particle[hit[6]] = []

                unique_particle[hit[6]] += [hit[:6]]

            # i0 - usable track number
            i0 = 0
            # i1 - all the available tracks
            i1 = 0
            # i2 - number of tracks passed 'number of hits' cut
            i2 = 0

            # output file with all the needed information about the track
            output_file = TFile.Open("mcparticle_data/" + str(run_number) + "_" + str(event_number) + "_FThits.root", "RECREATE")
            ft_ntuple = [TNtuple(name, name, "x:y:z:track") for name in ["s0l0", "s0l3", "s1l0", "s1l3", "s2l0", "s2l3"]]
            ft_full_ntuple = [TNtuple(name, name, "x:y:z:track") for name in ["full_s0l0", "full_s0l3", "full_s1l0", "full_s1l3", "full_s2l0", "full_s2l3"]]
            line_ntuple = TNtuple("line", "upstream track's line parameters", "kx:x0:ky:y0:z0:p:q:track")

            for key in unique_particle.keys():
                i1 += 1

                particle = np.asarray(unique_particle[key]).T

                # all the hits information
                if len(particle[2][particle[2] > 7000]) == 0: continue
                ft_full = particle.T[particle[2] > 7000]
                z_limits_ft = [7810., 7840., 8020., 8050., 8490., 8520., 8700., 8730., 9180., 9210., 9390., 9420.]

                for ft_one in ft_full:
                    for i in range(6):
                        if (ft_one[2] > z_limits_ft[2*i]) and (ft_one[2] < z_limits_ft[2*i+1]):
                            if is_inside(ft_one[0], ft_one[1]):
                                _x = smearing_mt_m_x(ft_one[0])
                                _y = smearing_mt_m_y(ft_one[1])
                                # if not is_inside(_x, _y):
                                #     print(f'Hit was inside. X {ft_one[0]}:{_x}, Y {ft_one[1]}:{_y}')
                            else:
                                _x = smearing_mt_o_x(ft_one[0])
                                _y = smearing_mt_o_y(ft_one[1])
                                # if is_inside(_x, _y):
                                #     print(f'Hit was outside. X {ft_one[0]}:{_x}, Y {ft_one[1]}:{_y}')

                            ft_full_ntuple[i].Fill(_x, _y, ft_one[2], key)

                if 11 in np.abs(particle[5]): continue

                # cuts on number of hits in different stations
                if len(particle[2][particle[2] < 1000]) < 2: continue
                if len(particle[2][(particle[2] < 3000) & (particle[2] > 2000)]) < 2: continue
                if len(particle[2][particle[2] > 7000]) != 6: continue

                i2 += 1

                velo = particle.T[particle[2] < 1000]
                ft = particle.T[particle[2] > 7000]
                ut = particle.T[(particle[2] < 3000) & (particle[2] > 2000)]

                # _dp = np.abs(ut[-1][3] - ft[0][3])
                # # applied cuts on dp
                # if _dp > 7.: continue

                for k in range(6):
                    # ft hits
                    ft_ntuple[k].Fill(ft[k][0], ft[k][1], ft[k][2], key)

                # upstream track information
                _1dz = 1. / (ut[-1][2] - velo[0][2])
                _x = smearing_ut_x(ut[-1][0])
                _y = smearing_ut_y(ut[-1][1])
                _kx = (_x - smearing_velo_xy(velo[0][0])) * _1dz
                _ky = (_y - smearing_velo_xy(velo[0][1])) * _1dz
                line_ntuple.Fill(_kx, _x, _ky, _y, ut[-1][2], ut[-1][3], ut[-1][4], key)

                i0 += 1

            output_file.Write()
            output_file.Close()
            del tree


def processing(_run_number, _event_number, _x_sigma, track):
    # open preprocessed data file
    process_file = up.open("mcparticle_data/" + str(_run_number) + "_" + str(_event_number) + "_FThits.root")

    #                                                      0  1  2  3  4  5 6  7
    # TNtuple("line", "upstream track's line parameters", "kx:x0:ky:y0:z0:p:q:track")
    # tracks = np.asarray(process_file["line"].arrays(["*"], outputtype=tuple)).T

    layers = ["full_s0l0", "full_s0l3", "full_s1l0", "full_s1l3", "full_s2l0", "full_s2l3"]
    hits = {}
    for i in range(6):
        # [TNtuple(name, name, "x:y:z:track") for name in ["full_s0l0", "full_s0l3", "full_s1l0", "full_s1l3", "full_s2l0", "full_s2l3"]]
        hits[i] = np.asarray(process_file[layers[i]].arrays(["*"], outputtype=tuple)).T

    layer_z_positions = [7826.13, 8035.95,\
                         8508.14, 8717.93,\
                         9193.17, 9402.96]

    x_correction_fit_param = [-1.0630336, 6.7305133] # start layer 0
    # x_correction_fit_param = [-1.0862869, 7.0042774]
    y_searchin_window_fit_param = [-1.03523872, 4.62380112]
    # y_searchin_window_fit_param = [-1.1290717, 5.0527662]
    y_searchin_window_limit_min_max = 75.

    # print(f"\n>>>> RUN: {_run_number}\n")
    reconstructed_tracks = {}
    empty_hit = np.array([0, 0, 0, -1])

    # for track in tracks:
    # total
    # i[0] += 1

    # track parameters
    track_p    = smearing_p(np.abs(track[5]))
    track_kx   = track[0]
    track_ky   = track[2]
    track_x0   = track[1]
    track_y0   = track[3]
    track_z0   = track[4]
    track_num  = track[7]
    track_q    = track[6]

    i_out = []
    out = []
    magnet_pos = []
    # position of the kink plane (magnet position)
    for mag in range(3):
        # total pcut incut   01    01+   01++ 01+++ 012345 w/ghost
        i = [0, 0, 0, 0, 0, 0, 0, 0, 0]
        if mag == 0:
            magnet_z_position = 5282.5
        elif mag == 1:
            z_magnet_parameters = np.array([5212.38, 406.609, -1102.35, -498.039])
            magnet_z_position = z_magnet_parameters[0] + z_magnet_parameters[2] * track_kx * track_kx + z_magnet_parameters[3] * track_ky * track_ky
        elif mag == 2:
            alpha = np.array([5363.5, -3615.15, 399.87, 97205., -0.01081, -0.15562, 1624.3])
            magnet_z_position = alpha[0] + alpha[1] * track_ky * track_ky + alpha[2] * track_kx * track_kx + alpha[3] / track_p +\
                                alpha[4] * np.abs(track_x0) + alpha[5] * np.abs(track_y0) + alpha[6] * np.abs(track_ky)

        reconstructed_tracks[track_num] = []
        reconstracted_segments = ()

        # p cut for the low momentum particles
        # if track_p < 2.e3: continue
        # pcut
        i[1] += 1

        # first layer of the first station
        layer_num = 0

        prediction = []
        # x prediction and searching window estimation
        x_at_s0l0_prediction = track_kx * (layer_z_positions[layer_num] - track_z0) + track_x0

        if track_q > 0:
            # using when the p cut is off
            if track_p * (1. - _x_sigma * 0.2) < 1000.:
                x_at_s0l0_searchin_window_min = x_at_s0l0_prediction - func(1000., x_correction_fit_param[0], x_correction_fit_param[1])
            else:
                x_at_s0l0_searchin_window_min = x_at_s0l0_prediction - func(track_p * (1. - _x_sigma * 0.2), x_correction_fit_param[0], x_correction_fit_param[1])
            x_at_s0l0_searchin_window_max = x_at_s0l0_prediction - func(track_p * (1. + _x_sigma * 0.2), x_correction_fit_param[0], x_correction_fit_param[1])
        else:
            if track_p * (1. - _x_sigma * 0.2) < 1000.:
                x_at_s0l0_searchin_window_max = x_at_s0l0_prediction + func(1000., x_correction_fit_param[0], x_correction_fit_param[1])
            else:
                x_at_s0l0_searchin_window_max = x_at_s0l0_prediction + func(track_p * (1. - _x_sigma * 0.2), x_correction_fit_param[0], x_correction_fit_param[1])
            x_at_s0l0_searchin_window_min = x_at_s0l0_prediction + func(track_p * (1. + _x_sigma * 0.2), x_correction_fit_param[0], x_correction_fit_param[1])

        x_at_s0l0_prediction -= track_q * func(track_p, x_correction_fit_param[0], x_correction_fit_param[1])

        if x_at_s0l0_searchin_window_min < -3200.:
            x_at_s0l0_searchin_window_min = -3200.
        if x_at_s0l0_searchin_window_max > 3200.:
            x_at_s0l0_searchin_window_max = 3200.

        prediction.append(x_at_s0l0_searchin_window_min)
        prediction.append(x_at_s0l0_prediction)
        prediction.append(x_at_s0l0_searchin_window_max)

        # y searching window estimation
        y_at_s0l0_searchin_window_min_max = func(track_p, y_searchin_window_fit_param[0], y_searchin_window_fit_param[1]) + 10.
        if track_p < 1.e4:
            y_at_s0l0_searchin_window_min_max -= 0.0025 * (track_p - 12000.)

        if y_at_s0l0_searchin_window_min_max > y_searchin_window_limit_min_max:
            y_at_s0l0_searchin_window_min_max = y_searchin_window_limit_min_max

        y_at_s0l0_prediction = track_ky * (layer_z_positions[layer_num] - track_z0) + track_y0
        y_at_s2l3_prediction = track_ky * (layer_z_positions[-1] - track_z0) + track_y0
        y_at_s0l0_searchin_window_min = y_at_s0l0_prediction - y_at_s0l0_searchin_window_min_max
        y_at_s0l0_searchin_window_max = y_at_s0l0_prediction + y_at_s0l0_searchin_window_min_max
        y_at_s2l3_searchin_window_min = y_at_s2l3_prediction - y_at_s0l0_searchin_window_min_max
        y_at_s2l3_searchin_window_max = y_at_s2l3_prediction + y_at_s0l0_searchin_window_min_max

        prediction.append(y_at_s0l0_searchin_window_min)
        prediction.append(y_at_s0l0_prediction)
        prediction.append(y_at_s0l0_searchin_window_max)

        #######
        prediction.append(y_at_s2l3_searchin_window_min)
        prediction.append(y_at_s2l3_searchin_window_max)
        #######

        # hits inside the searching window calculated above
        hits_at_s0l0_cut = hits[layer_num][
        ((hits[layer_num][:,0] > x_at_s0l0_searchin_window_min) & (hits[layer_num][:,0] < x_at_s0l0_searchin_window_max)) &\
        ((hits[layer_num][:,1] > y_at_s0l0_searchin_window_min) & (hits[layer_num][:,1] < y_at_s0l0_searchin_window_max))]

        # prediction for the kink point. Will be used in the next steps
        x_at_magnet_prediction = (magnet_z_position - track_z0) * track_kx + track_x0

        inverted_d_z_magnet_s0l0 = 1. / (layer_z_positions[layer_num] - magnet_z_position)

        # print(f"track:{track_num}")
        # print(f"incut:\n{hits_at_s0l0_cut}")
        # print(len(hits_at_s0l0_cut))

        for hit_at_s0l0 in hits_at_s0l0_cut:
            x_at_s0l0_hit = hit_at_s0l0[0]
            y_at_s0l0_hit = hit_at_s0l0[1]

            y_hits = [y_at_s0l0_hit]
            layer  = [layer_num]

            # search for the hit in the last layer of the first station
            layer_num = 1

            d_z_s0l3_s0l0 = layer_z_positions[1] - layer_z_positions[0]
            # prediction of the y position
            # d_slope = (y_at_s0l0_hit - y_at_s0l0_prediction) * inverted_d_z_magnet_s0l0
            # y_shift = (y_hit_at_s0l0 - y_at_s0l0_prediction) * (1. + d_slope)
            y_shift = (y_at_s0l0_hit - y_at_s0l0_prediction) * (layer_z_positions[1] - track_z0) / (layer_z_positions[0] - track_z0)
            y_at_s0l3_prediction = (layer_z_positions[1] - track_z0) * track_ky + track_y0 + y_shift
            # rough estimation of the expected slope
            kx = (x_at_s0l0_hit - x_at_magnet_prediction) * inverted_d_z_magnet_s0l0
            # this is a small correction mainly for lower momentum tracks for which the zkink isn't perfect
            # and they'll have some bending. This is solely empirical.
            x_s0l3_curve = (kx - track_kx) * (-0.231615 + (track_kx * 33.1256 + kx * 10.4693) * (kx - track_kx))
            # the x-prediction for the s0l3 (last layer of the very first station)
            x_at_s0l3_prediction = x_at_s0l0_hit + kx * d_z_s0l3_s0l0 + x_s0l3_curve
            if np.abs(x_at_s0l3_prediction) > 3200.:
                # print('Out of the station')
                continue   # out of the station

            t = [1, 0, 0, 0, 0, 0]
            if hit_at_s0l0[3] == track_num:
                # incut
                i[2] += 1

            reconstracted_segment = ()
            reconstracted_segment += (hit_at_s0l0,)
            rejection = 0

            # find the best s0l3 hit candidate
            hit_at_s0l3 = best_hit(x_at_s0l3_prediction, y_at_s0l3_prediction, np.sign(track_ky), y_at_s0l0_hit, np.sign(kx), x_at_s0l0_hit, layer_num, hits)
            # parameters of the found hit
            if not hit_at_s0l3:
                x_at_s0l3_hit = x_at_s0l3_prediction
                y_at_s0l3_hit = y_at_s0l3_prediction
                reconstracted_segment += (empty_hit,)
                rejection += 1
            else:
                x_at_s0l3_hit = hit_at_s0l3[0][0]
                y_at_s0l3_hit = hit_at_s0l3[0][1]
                reconstracted_segment += (hit_at_s0l3[0],)
                t[layer_num] = 1

            if hit_at_s0l3:
                if hit_at_s0l0[3] == track_num and hit_at_s0l3[0][3] == track_num:
                    # 01
                    i[3] += 1

            i2345 = [0, 0, 0, 0]

            # print(f"2d best: {best_hit(x_at_s0l3_prediction, y_at_s0l3_prediction, layer_num, hits)}")
            # print(f"x best: {best_hit_old(x_at_s0l3_prediction, y_at_s0l3_prediction, layer_num, hits)}")

            y_hits.append(y_at_s0l3_hit)
            layer.append(layer_num)

            # recalculate track parameters based on existed hits
            kx_calculated = (x_at_s0l3_hit - x_at_s0l0_hit) / d_z_s0l3_s0l0

            # d_kx = kx_calculated - track_kx
            # TODO: check this 0.011, calculate coordinate difference
            #       for the giving distance in between two layers
            # abs_d_kx = max(0.011, np.abs(d_kx)

            # if hit_at_s0l0[3] == track_num:
            #     print(f"0:{hit_at_s0l0}")
            #     if hit_at_s0l3:
            #         print(f"1:{hit_at_s0l3[0]}")

            x_at_layer_hit = x_at_s0l3_hit
            for layer_num in range(2, 6, 1):
                # TODO: find corrections to obtain the "ext_a_b_g_matrix" or
                #       parametrize curve like the "x_s0l3_curve"
                x_layer_curve = 0.
                x_at_layer_prediction = x_at_s0l0_hit + kx_calculated * (layer_z_positions[layer_num] - layer_z_positions[0]) + x_layer_curve

                ky = (y_hits[-1] - y_hits[-2]) / (layer_z_positions[layer[-1]] - layer_z_positions[layer[-2]])
                y_at_layer_prediction = ky * (layer_z_positions[layer_num] - layer_z_positions[layer_num - 1]) + y_hits[-1]

                hit_at_layer = best_hit(x_at_layer_prediction, y_at_layer_prediction, np.sign(track_ky), y_hits[-1], np.sign(kx_calculated), x_at_layer_hit, layer_num, hits)
                # parameters of the found hit
                if not hit_at_layer:
                    x_at_layer_hit = x_at_layer_prediction
                    y_at_layer_hit = y_at_layer_prediction
                    reconstracted_segment += (empty_hit,)
                    rejection += 1
                else:
                    x_at_layer_hit = hit_at_layer[0][0]
                    y_at_layer_hit = hit_at_layer[0][1]
                    reconstracted_segment += (hit_at_layer[0],)
                    t[layer_num] = 1


                if hit_at_layer:
                    if hit_at_s0l0[3] == track_num and hit_at_layer[0][3] == track_num:
                        i2345[layer_num - 2] = 1

                y_hits.append(y_at_layer_hit)
                layer.append(layer_num)

                # if hit_at_s0l0[3] == track_num and hit_at_layer:
                #     print(f"{layer_num}:{hit_at_layer[0]}")

            if i2345 == [1, 1, 1, 1]:
                i[7] += 1
            if i2345.count(0) == 1:
                i[6] += 1
            if i2345.count(0) == 2:
                i[5] += 1
            if i2345.count(0) == 3:
                i[4] += 1

            if t.count(1) >= 4:
                i[8] += 1

            # if less than 4 hits were found it is not a track we are interested in anymore
            if rejection > 2:
                continue
            reconstructed_tracks[track_num] += [reconstracted_segment]


        i_out.append(i)
        out.append(np.asarray(reconstructed_tracks[track_num]))
        magnet_pos.append(magnet_z_position)
        magnet_pos.append(x_at_magnet_prediction)

        # for key, value in reconstructed_tracks.items():
        #     reconstructed_tracks[key] = np.asarray(value)



    # return i, reconstructed_tracks
    return i_out, out, magnet_pos, prediction


def event_display(_run_number = None, _event_number = None, _to_save = False, _verbosity = 0):

    if _run_number is None:
        _run_number = 2500
    if _event_number is None:
        _event_number = 1

    # open preprocessed data file
    process_file = up.open("mcparticle_data/" + str(_run_number) + "_" + str(_event_number) + "_FThits.root")

    #                                                      0  1  2  3  4  5 6  7
    # TNtuple("line", "upstream track's line parameters", "kx:x0:ky:y0:z0:p:q:track")
    tracks = np.asarray(process_file["line"].arrays(["*"], outputtype=tuple)).T

    layers = ["s0l0", "s0l3", "s1l0", "s1l3", "s2l0", "s2l3"]
    hits = {}
    for i in range(6):
        # [TNtuple(name, name, "x:y:z:track") for name in ["full_s0l0", "full_s0l3", "full_s1l0", "full_s1l3", "full_s2l0", "full_s2l3"]]
        hits[i] = np.asarray(process_file[layers[i]].arrays(["*"], outputtype=tuple)).T

    layer_z_positions = [7826.13, 8035.95,\
                         8508.14, 8717.93,\
                         9193.17, 9402.96]

    _x_sigma = 3.

    x_correction_fit_param = [-1.0630336, 6.7305133] # start layer 0
    # x_correction_fit_param = [-1.0862869, 7.0042774]
    y_searchin_window_fit_param = [-1.03523872, 4.62380112]
    # y_searchin_window_fit_param = [-1.1290717, 5.0527662]
    y_searchin_window_limit_min_max = 75.

    magnet_z_position = 5282.5

    # print("\n")
    for track, l0, l1, l2, l3, l4, l5 in zip(tracks, hits[0], hits[1], hits[2], hits[3], hits[4], hits[5]):
        track_p    = smearing_p(np.abs(track[5]))
        track_kx   = track[0]
        track_ky   = track[2]
        track_x0   = track[1]
        track_y0   = track[3]
        track_z0   = track[4]
        track_num  = track[7]
        track_q    = track[6]
        if track_p < 2e3: continue
        if _verbosity:
            print(f'\ntrack_p = {track_p:.1f} MeV/c')

        x = np.array([track_x0,  l0[0], l1[0], l2[0], l3[0], l4[0], l5[0]])
        y = np.array([track_y0,  l0[1], l1[1], l2[1], l3[1], l4[1], l5[1]])
        z = np.array([track_z0,  l0[2], l1[2], l2[2], l3[2], l4[2], l5[2]])
        n = np.array([track_num, l0[3], l1[3], l2[3], l3[3], l4[3], l5[3]])
        fit_z = np.linspace(2000, 9500, 100, endpoint=True)

        i_out, out, magnet_pos, prediction = processing(_run_number, _event_number, _x_sigma, track)

        fit_cut = 0

        if _verbosity:
            print("Before all the cuts:")
            print(f"i: wcut, 01, 01x, 01xx, 01xxx, full, >4hits")
            print(f"i: {i_out[1][2:]}")

        plt.figure(figsize = [13, 4])
        ax1 = plt.subplot(1,2,1)
        ax1.set_title('x(z)')
        ax1.set_xlabel('z [mm]')
        ax1.set_ylabel('x [mm]')
        ax1.scatter(z, x, marker = 'o')

        xopt, xcov = optimize.curve_fit(parabola, z, x)
        # print(f"err: {np.sqrt(np.diag(xcov))}")
        ax1.plot(fit_z, parabola(fit_z, *xopt), 'g-')

        # xopt, xcov = optimize.curve_fit(parabola1, z, x)
        # print(f"err: {np.sqrt(np.diag(xcov))}")
        # plt.plot(fit_z, parabola1(fit_z, *xopt), 'c--')

        # xopt, xcov = optimize.curve_fit(parabola2, z, x)
        # print(f"err: {np.sqrt(np.diag(xcov))}")
        # plt.plot(fit_z, parabola2(fit_z, *xopt), 'b--')

        # print(f"init x cov: \n{xcov}\nerr: {np.sqrt(np.diag(xcov))}")
        # print(xopt)
        x_min = np.array([track[1], 0., 0.])
        x_max = np.array([track[1], 0., 0.])
        z_x_lim = np.array([track[4], 0., 7826.])
        x_pred = np.array([track[1], 0., 0.])
        color = ['r-', 'm-', 'c-']
        color1 = ['r--', 'm--', 'c--']
        # for i in range(3):
        for i in [1]:
            z_x_lim[1] = magnet_pos[2*i]
            x_min[1] = magnet_pos[2*i+1]
            x_min[2] = prediction[0]
            x_pred[1] = magnet_pos[2*i+1]
            x_pred[2] = prediction[1]
            x_max[1] = magnet_pos[2*i+1]
            x_max[2] = prediction[2]
            ax1.plot(z_x_lim, x_min, color[i])
            ax1.plot(z_x_lim, x_max, color[i])
            ax1.plot(z_x_lim, x_pred, color1[i])

        ax2 = plt.subplot(1,2,2)
        ax2.set_title('y(z)')
        ax2.set_xlabel('z [mm]')
        ax2.set_ylabel('y [mm]')
        ax2.scatter(z, y, marker = 'o')
        # yopt, ycov = optimize.curve_fit(parabola, z, y)
        # print(f"init y cov: \n{ycov}\nerr: {np.sqrt(np.diag(ycov))}")
        # print(yopt)
        # plt.plot(fit_z, parabola(fit_z, *yopt), 'g-')
        y_min = np.array([track[3], prediction[3]])
        y_max = np.array([track[3], prediction[5]])
        z_y_lim = np.array([track[4], 7826.])
        y_pred = np.array([track[3], prediction[4]])
        ax2.plot(z_y_lim, y_min, 'r-')
        ax2.plot(z_y_lim, y_max, 'r-')
        ax2.plot(z_y_lim, y_pred, 'r--')

        for p in out[1]:
            p = p[p[:,3] != -1]

            z_p = np.append(np.array([track_z0]), p[:,2])
            x_p = np.append(np.array([track_x0]), p[:,0])
            xopt, xcov = optimize.curve_fit(parabola, z_p, x_p)
            tmpo, tmpc = optimize.curve_fit(parabola, z_x_lim, x_pred)

            y_p = np.append(np.array([track_y0]), p[:,1])

            if np.sign(tmpo[0]) != np.sign(xopt[0]):
                if _verbosity == 2:
                    print('skipped due to sign')
                i_out[1][8] -= 1
                continue
            if (np.sign(tmpo[0]) > 0 and parabola(z_x_lim[1], *xopt) < x_pred[1]) or\
               (np.sign(tmpo[0]) < 0 and parabola(z_x_lim[1], *xopt) > x_pred[1]):
                if _verbosity == 2:
                    print('skipped due to parabola curvature')
                i_out[1][8] -= 1
                continue
            if y[-1] >= prediction[6] and y[-1] <= prediction[7]:
                if p[-1,1] < prediction[6] or p[-1,1] > prediction[7]:
                    if _verbosity == 2:
                        print('skipped, y is out')
                    i_out[1][8] -= 1
                    continue

            chi2_x_opt, chi2_x_cov = optimize.curve_fit(parabola, z_p[1:], x_p[1:])
            chi2_y_opt, chi2_y_cov = optimize.curve_fit(straight_line, z_p[1:], y_p[1:])

            chi2 = ((chi2_y_opt[0] - track_ky) / 0.00205408158941)**2 + ((straight_line(z_p[1], *chi2_y_opt) - y_pred[1]) / 6.01608821143)**2 + ((parabola(z_p[1], *chi2_x_opt) - x_pred[2]) / 4.75133640143)**2
            if chi2 > 60:
                if _verbosity == 2:
                    print('skipped, \uAB53\u00B2 > 60')
                i_out[1][8] -= 1
                continue

            if _verbosity == 2:
                print(f"chi2: {chi2:.1f}")

            ax1.scatter(p[:,2], p[:,0], marker = '.', color='y')
            # print(f"\nx cov: \n{xcov}\nerr: {np.sqrt(np.diag(xcov))}")
            # print(xopt)
            ax1.plot(fit_z, parabola(fit_z, *xopt), 'y--')
            # ax1.plot(p[:,2], p[:,0], color1[i])

            ax2.scatter(p[:,2], p[:,1], marker = '.', color='y')
            yopt, ycov = optimize.curve_fit(parabola, z_p, y_p)
            # print(f"y cov: \n{ycov}\nerr: {np.sqrt(np.diag(ycov))}")
            # print(yopt)
            ax2.plot(fit_z, parabola(fit_z, *yopt), 'y--')

        if _verbosity:
            print("After all the cuts:")
            print(f"i: wcut, 01, 01x, 01xx, 01xxx, full, >4hits")
            print(f"i: {i_out[1][2:]}")

        if _to_save:
            plt.savefig(f'output/particle{int(track_num)}_{_run_number}_{_event_number}.pdf')
        plt.show()

        if input('Finish? [y/n]\n') == 'y':
            break
        else:
            pass


def execute_this_please(_run_number = None, _event_number = None, _to_preprocess = False, _verbosity = 0):

    count_reconstructed = 0
    count_reconstructable = 0
    count_true_reconstructed = 0

    count_reconstructed_as_signal = 0
    count_reconstructed_as_clone = 0
    count_inside_x = 0
    count_inside_y = 0
    count_inside_xy = 0

    count_removed_sign = 0
    count_removed_curv = 0

    output = np.array([0, 0, 0, 0, 0, 0, 0, 0, 0])

    outfile = ROOT.TFile.Open("output/out_tracking.root", "RECREATE")
    res_ntuple = ROOT.TNtuple("result", "", "run:event:reconstructable:4hits:5hits:6hits:reconstructed")
    plot_ntuple = ROOT.TNtuple("track", "", "corr:chi2:track")

    if _run_number is None:
        _run_number = np.array([2500, 2501, 2503, 2504, 2505])
    if _event_number is None:
        _event_number = np.arange(1, 51, 1, dtype = int)

    if not isinstance(_run_number, np.ndarray) and not isinstance(_run_number, list):
        _run_number = np.array([_run_number])

    if not isinstance(_event_number, np.ndarray) and not isinstance(_event_number, list):
        _event_number = np.array([_event_number])

    for run in _run_number: # TODO: change it accordigly
        for event in _event_number: # TODO: change it accordigly
    # for run in [2500]:
    #     for event in [1]:

            if _to_preprocess:
                preproc_ntuple(_run_number = run, _event_number = event)
            # open preprocessed data file
            process_file = up.open("mcparticle_data/" + str(run) + "_" + str(event) + "_FThits.root")

            #                                                      0  1  2  3  4  5 6  7
            # TNtuple("line", "upstream track's line parameters", "kx:x0:ky:y0:z0:p:q:track")
            tracks = np.asarray(process_file["line"].arrays(["*"], outputtype=tuple)).T

            layers = ["s0l0", "s0l3", "s1l0", "s1l3", "s2l0", "s2l3"]
            hits = {}
            for i in range(6):
                # [TNtuple(name, name, "x:y:z:track") for name in ["full_s0l0", "full_s0l3", "full_s1l0", "full_s1l3", "full_s2l0", "full_s2l3"]]
                hits[i] = np.asarray(process_file[layers[i]].arrays(["*"], outputtype=tuple)).T

            _x_sigma = 3.

            x_correction_fit_param = [-1.0630336, 6.7305133] # start layer 0
            # x_correction_fit_param = [-1.0862869, 7.0042774]
            y_searchin_window_fit_param = [-1.03523872, 4.62380112]
            # y_searchin_window_fit_param = [-1.1290717, 5.0527662]
            y_searchin_window_limit_min_max = 75.

            magnet_z_position = 5282.5

            for track, l0, l1, l2, l3, l4, l5 in zip(tracks, hits[0], hits[1], hits[2], hits[3], hits[4], hits[5]):
                if np.abs(track[5]) < 2e3: continue
                result = processing(run, event, _x_sigma, track)

                output = output + np.array(result[0][1])

                count_reconstructable = count_reconstructable + 1
                count_reconstructed = count_reconstructed + result[0][1][-1]
                count_true_reconstructed = count_true_reconstructed + 1 if result[0][1][-2] or result[0][1][-3] or result[0][1][-4] else count_true_reconstructed

                x = np.array([l0[0], l1[0], l2[0], l3[0], l4[0], l5[0]])
                y = np.array([l0[1], l1[1], l2[1], l3[1], l4[1], l5[1]])
                z = np.array([l0[2], l1[2], l2[2], l3[2], l4[2], l5[2]])
                num = l0[3]

                xopt, xcov = optimize.curve_fit(parabola, z, x)
                xerr = np.sqrt(np.diag(xcov))
                yopt, ycov = optimize.curve_fit(straight_line, z, y)
                yerr = np.sqrt(np.diag(ycov))

                # print(xopt)
                # print(xerr)
                # print(yopt)
                # print(yerr)
                # break

                m_factor = 1.
                n_factor = 1.

                if result[0][1][-2] or result[0][1][-3] or result[0][1][-4]:

                    p = result[1][1].T[:].T[result[1][1].T[3].T[:,0] == num][0].T
                    x_corr_popt = xopt
                    x_corr_pcov = xcov
                    y_corr_popt = yopt
                    y_corr_pcov = ycov

                    x_corr_popt, x_corr_pcov = optimize.curve_fit(parabola, p[2], p[0])
                    y_corr_popt, y_corr_pcov = optimize.curve_fit(straight_line, p[2], p[1])

                for p in result[1][1]:
                    p = p[p[:,3] != -1]

                    x_p = p[:,0]
                    y_p = p[:,1]
                    z_p = p[:,2]
                    num_p = p[:,3]

                    corr = 0
                    if (num_p == num).sum() > 3:
                        corr = 1

                    inside_x = False
                    inside_y = False

                    x_popt, x_pcov = optimize.curve_fit(parabola, z_p, x_p)
                    y_popt, y_pcov = optimize.curve_fit(straight_line, z_p, y_p)

                    # CHI2 PART NEEDS TO BE RE-WRITEN BACK TO TAKE INTO ACCOUNT ALL THE LAYERS i.e. FIT - HIT (see note eq 8)
                    chi2 = ((y_popt[0] - track[2]) / 0.00205408158941)**2 + ((straight_line(z_p[0], *y_popt) - result[3][4]) / 6.01608821143)**2 + ((parabola(z_p[0], *x_popt) - result[3][1]) / 4.75133640143)**2

###############

                    z_gr_p = np.append(np.array([track[4]]), p[:,2])
                    x_gr_p = np.append(np.array([track[1]]), p[:,0])

                    x_min = np.array([track[1], 0., 0.])
                    x_max = np.array([track[1], 0., 0.])
                    z_x_lim = np.array([track[4], result[2][2], 7826.])
                    x_pred = np.array([track[1], result[2][3], result[3][1]])

                    x_gr_opt, x_gr_cov = optimize.curve_fit(parabola, z_gr_p, x_gr_p)
                    t_gr_mpo, t_gr_mpc = optimize.curve_fit(parabola, z_x_lim, x_pred)

                    if np.sign(t_gr_mpo[0]) != np.sign(x_gr_opt[0]):
                        count_removed_sign = count_removed_sign + 1
                    if (np.sign(t_gr_mpo[0]) > 0 and parabola(z_x_lim[1], *x_gr_opt) < x_pred[1]) or\
                       (np.sign(t_gr_mpo[0]) < 0 and parabola(z_x_lim[1], *x_gr_opt) > x_pred[1]):
                        count_removed_curv = count_removed_curv + 1

###############

                    if result[0][1][-2] or result[0][1][-3] or result[0][1][-4]:
                        posible_ghost_as_signal = False

                        xopt = x_corr_popt
                        yopt = y_corr_popt
                        xerr = np.sqrt(np.diag(x_corr_pcov))
                        yerr = np.sqrt(np.diag(y_corr_pcov))

                        if x_popt[0] < xopt[0] + n_factor * xerr[0] and x_popt[0] > xopt[0] - n_factor * xerr[0] and x_popt[1] < xopt[1] + n_factor * xerr[1] and x_popt[1] > xopt[1] - n_factor * xerr[1] and x_popt[2] < xopt[2] + n_factor * xerr[2] and x_popt[2] > xopt[2] - n_factor * xerr[2]:
                            inside_x = True
                        if y_popt[0] < yopt[0] + m_factor * yerr[0] and y_popt[0] > yopt[0] - m_factor * yerr[0] and y_popt[1] < yopt[1] + m_factor * yerr[1] and y_popt[1] > yopt[1] - m_factor * yerr[1]:
                            inside_y = True

                        # if count_reconstructed_as_clone == 0:
                            # print(f'Xerr as signal:{xerr}')
                            # print(f'Yerr as signal:{yerr}')
                    else:
                        posible_ghost_as_signal = True

                        if x_popt[0] < xopt[0] + n_factor * xerr[0] and x_popt[0] > xopt[0] - n_factor * xerr[0] and x_popt[1] < xopt[1] + n_factor * xerr[1] and x_popt[1] > xopt[1] - n_factor * xerr[1] and x_popt[2] < xopt[2] + n_factor * xerr[2] and x_popt[2] > xopt[2] - n_factor * xerr[2]:
                            inside_x = True
                        if y_popt[0] < yopt[0] + n_factor * yerr[0] and y_popt[0] > yopt[0] - n_factor * yerr[0] and y_popt[1] < yopt[1] + n_factor * yerr[1] and y_popt[1] > yopt[1] - n_factor * yerr[1]:
                            inside_y = True

                        # print(f'Xerr as signal:{xerr}')
                        # print(f'Yerr as signal:{yerr}')


                    if inside_x:
                        count_inside_x = count_inside_x + 1
                    if inside_y:
                        count_inside_y = count_inside_y + 1
                    if inside_x and inside_y:
                        count_inside_xy = count_inside_xy + 1

                    if posible_ghost_as_signal and inside_x and inside_y:
                        count_reconstructed_as_signal = count_reconstructed_as_signal + 1

                    if not posible_ghost_as_signal and inside_x and inside_y:
                        count_reconstructed_as_clone = count_reconstructed_as_clone + 1

                    xopt, xcov = optimize.curve_fit(parabola, z, x)
                    xerr = np.sqrt(np.diag(xcov))
                    yopt, ycov = optimize.curve_fit(straight_line, z, y)
                    yerr = np.sqrt(np.diag(ycov))

                    # "xa:xb:xc:xerra:xerrb:xerrc:yk:yb:yerrk:yerrb:xat:xbt:xct:ykt:ybt"
                    plot_ntuple.Fill(corr, chi2, num)
                # break
                # "4hits:5hits:6hits:reconstructed"
                res_ntuple.Fill(run, event, result[0][1][1], result[0][1][-4], result[0][1][-3], result[0][1][-2], result[0][1][-1])


    outfile.Write()
    outfile.Close()

    if _verbosity:
        print(f'Reconstructed: {count_reconstructed}')
        print(f'Reconstructable: {count_reconstructable}')
        print(f'True: {count_true_reconstructed}')
        print(f'Removed sign: {count_removed_sign}')
        print(f'Removed curve: {count_removed_curv}')
        print(f'R as Signal: {count_reconstructed_as_signal}')
        print(f'R as Clone: {count_reconstructed_as_clone}')
        print(f'In X: {count_inside_x}')
        print(f'In Y: {count_inside_y}')
        print(f'In XY: {count_inside_xy}')
        print(f'Output: {output}')


import argparse

if __name__ == '__main__':

    run_number = np.array([2500, 2501, 2503, 2504, 2505])
    event_number = np.arange(1, 51, 1, dtype = int)

    parser = argparse.ArgumentParser(description = 'Simple forward tracking script', formatter_class = argparse.ArgumentDefaultsHelpFormatter)
    # subparsers = parser.add_subparsers(required = True, dest = 'subparser') python3.7
    subparsers = parser.add_subparsers(dest = 'subparser')
    subparser1 = subparsers.add_parser('display')
    subparser1.add_argument('-r', dest = 'run', type = int, help = 'run number', required = False, default = run_number[0], choices = run_number)
    subparser1.add_argument('-e', dest = 'event', type = int, help = 'event number', required = False, default = event_number[0], choices = event_number)
    subparser1.add_argument('-s', dest = 'save', action = 'store_true', help = 'every event figure will be saved into the "output/" folder', required = False)
    subparser1.add_argument('-v', dest = 'verbosity', type = int, help = 'verbosity level', required = False, default = 0, choices = [0, 1, 2])
    subparser2 = subparsers.add_parser('execute')
    subparser2.add_argument('-r', dest = 'run', type = int, nargs = '*', help = 'run number(s)', required = False, default = run_number, choices = run_number)
    subparser2.add_argument('-e', dest = 'event', type = int, nargs = '*', help = 'event number(s)', required = False, default = event_number, choices = event_number)
    subparser2.add_argument('-p', dest = 'preprocess', action = 'store_true', help = 'pre-procession will be done automatically', required = False)
    subparser2.add_argument('-v', dest = 'verbosity', type = int, help = 'verbosity level', required = False, default = 0, choices = [0, 1])
    subparser3 = subparsers.add_parser('preprocess')
    subparser3.add_argument('-r', dest = 'run', type = int, nargs = '*', help = 'run number(s)', required = False, default = run_number, choices = run_number)
    subparser3.add_argument('-e', dest = 'event', type = int, nargs = '*', help = 'event number(s)', required = False, default = event_number, choices = event_number)
    arguments = parser.parse_args()

    print('Let the game begin!')
    if arguments.subparser:
        print(f'Using {arguments.subparser} mode.')
        print(f'Run: {arguments.run}')
        print(f'Event: {arguments.event}')
    else:
        print(f'Please, do: python tracking.py -h')

    if arguments.subparser == 'display':
        if arguments.save:
            print(f'Every event will be saved into the "output/" folder.')

        event_display(_run_number = arguments.run, _event_number = arguments.event, _to_save = arguments.save, _verbosity = arguments.verbosity)
    elif arguments.subparser == 'execute':
        execute_this_please(_run_number = arguments.run, _event_number = arguments.event, _to_preprocess = arguments.preprocess, _verbosity = arguments.verbosity)
    elif arguments.subparser == 'preprocess':
        preproc_ntuple(_run_number = arguments.run, _event_number = arguments.event)

    print('Fin.')


'''
m = n = 1
Reconstructed: 398630
Reconstructable: 87472
True: 79648
Removed sign: 29597
Removed curve: 38173
R as Signal: 0
R as Clone: 80399
In X: 84247
In Y: 84708
In XY: 80399
Output: [     0  87472  81583  81035    694   1246   2699  75704 398630]


m = n = 3
Reconstructed: 398521
Reconstructable: 87472
True: 79687
Removed sign: 29535
Removed curve: 38168
R as Signal: 10
R as Clone: 88067
In X: 102285
In Y: 96081
In XY: 88077
Output: [     0  87472  81639  81081    697   1249   2705  75734 398521]

'''

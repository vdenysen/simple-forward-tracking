import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import uproot

if __name__ == '__main__':
    file = uproot.open('../output/out_tracking.root')
    events = file['track']
    df = events.arrays(['corr', 'chi2'], library = 'pd')

    wrong = len(df.query('corr == 0'))
    correct = len(df.query('corr == 1'))

    rec = 87472
    e = []
    g = []
    cuts = []
    for cut in range(5, 200, 5):
        tmp = df.query(f'chi2 < {cut}')
        cuts.append(cut)
        e.append(100. * len(tmp.query('corr == 1')) / rec)
        g.append(100. * len(tmp.query('corr == 0')) / len(tmp.query('corr == 1')))


    plt.figure(figsize=(6,4))
    plt.gca().set(title='', xlabel=r'$\chi^2$ cut', ylabel='Efficiency / GhostRate [%]')
    plt.scatter(cuts, e, label = f'Efficiency')
    plt.scatter(cuts, g, label = f'GhostRate')
    # plt.yticks(n)

    plt.legend(loc = 'best')
    plt.savefig('../output/eff_gr_cut.pdf')
    plt.savefig('../output/eff_gr_cut.png')



    # file = uproot.open('out_tracking.root')
    # events = file['result']
    # df = events.arrays(['run', 'event', 'reconstructable', '4hits', '5hits', '6hits', 'reconstructed'], library = 'pd')
    # list = ['4hits', '5hits', '6hits']
    # df['a'] = df[list].sum(axis = 1)
    #
    # eff = []
    # gr = []
    # reconstr = df.query('a == 1')['reconstructed']
    # for run in [2500, 2501, 2503, 2504, 2505]:
    #     for event in range(1, 51, 1):
    #         df_tmp = df.query(f'run == {run} & event == {event}')
    #         eff.append(100. * (df_tmp['a'].sum() / df_tmp['reconstructable'].sum()))
    #         gr.append(100. * (df_tmp['reconstructed'].sum() - df_tmp['a'].sum()) / df_tmp['reconstructed'].sum())
    #
    #
    # t_eff = 100. * (df['a'].sum() / df['reconstructable'].sum())
    # t_gr = 100. * (df['reconstructed'].sum() - df['a'].sum()) / df['reconstructed'].sum()
    #
    # plt.figure(figsize=(6,4))
    # plt.gca().set(title='', xlabel='Efficiency / GhostRate [%]', ylabel='')
    # plt.hist(eff, bins = 50, alpha = 0.5, label = f'Efficiency, [{round(t_eff,2)} %]', density = True)
    # plt.hist(gr, bins = 50, alpha = 0.5, label = f'GhostRate, [{round(t_gr,2)} %]', density = True)
    # # plt.yticks(n)
    # plt.xticks(np.arange(50,100,5))
    # plt.legend(loc = 'best')
    # plt.savefig('../output/eff_gr.pdf')
    # plt.savefig('../output/eff_gr.png')
    #
    # plt.figure(figsize=(6,4))
    # plt.gca().set(title='', xlabel='Number of reconstructed tracks', ylabel='Number of events')
    # plt.hist(reconstr, bins = 50, alpha = 0.5)
    # plt.yscale('log', nonposy='clip')
    # plt.savefig('../output/reconstr.pdf')
    # plt.savefig('../output/reconstr.png')
    #
    # plt.show()

import uproot3 as up
import numpy as np
import matplotlib.pyplot as plt
from scipy import optimize
import ROOT
from ROOT import TCanvas, TFile, TProfile, TNtuple, TH1F, TH2F, TGraph
from ROOT import gROOT, gBenchmark, gRandom, gSystem

def parabola(_z, _a, _b, _c):
    """
    Parabola fit

    Args: _z = np.array([float]), z coordinate of the hit
    """
    return _a * _z * _z + _b * _z + _c

def parabola1(_z, _a, _b):
    """
    Parabola fit

    Args: _z = np.array([float]), z coordinate of the hit
    """
    return _a * (_z * _z + _b)

def parabola2(_z, _a, _b, _c):
    """
    Parabola fit

    Args: _z = np.array([float]), z coordinate of the hit
    """
    return _a * (_z + _b) * (_z + _b) + _c

def straight_line(_z, _k, _b):
    """
    Straight line fit

    Args: _z = np.array([float]), z coordinate of the hit
    """
    return _z * _k + _b

def func(_p, _k, _p0):
    """
    Correction of the x coordinate straight line estimation
    as a function of momentum of the particle

    Args: _p = np.array([float]), momentum of the particle
    """
    return (_p ** _k) * (10. ** _p0)

def func1(_p, _a, _b):
    """
    Correction of the x coordinate straight line estimation
    as a function of momentum of the particle

    Args: _p = np.array([float]), momentum of the particle
    """
    return _a / _p + _b

def dx_p(_layer_number, _save_root = 0, _fit = 0, _corrected = 1):
    if _save_root:
        output_file = TFile.Open("mcparticle_data/" + str(_layer_number) + "_dx_p.root", "RECREATE")
        out_ntuple = TNtuple("tuple", "tuple", "p:x_true:x_predicted:x_correction:q")

    p_plus  = ()
    dx_plus = ()
    p_minus  = ()
    dx_minus = ()

    # for run_number in [2500]:
    #     for event_number in [3]:
    for run_number in [2500, 2501, 2503, 2504, 2505]:
        for event_number in range(1, 51, 1):
            # open preprocessed data file
            process_file = up.open("mcparticle_data/" + str(run_number) + "_" + str(event_number) + "_FThits.root")

            #                                                       0  1  2  3  4 5 6   7
            # TNtuple("line", "upstream track's line parameters", "kx:x0:ky:y0:z0:p:q:track")
            tracks = np.asarray(process_file["line"].arrays(["*"], outputtype=tuple)).T

            layers = ["s0l0", "s0l3", "s1l0", "s1l3", "s2l0", "s2l3"]
            hits = np.asarray(process_file[layers[_layer_number]].arrays(["*"], outputtype=tuple)).T

            layer_z_positions = [7826.13, 8035.95,\
                                 8508.14, 8717.93,\
                                 9193.17, 9402.96]
            layer_z_position = layer_z_positions[_layer_number]

            if _corrected:
                fit_params = [[-1.06284798, 6.72940271], [-1.06735443, 6.77987984],\
                              [-1.075988,   6.88064182], [-1.0793602,  6.92087843],\
                              [-1.08581074, 7.00223321], [-1.08853011, 7.03555771]]

                fit_param = fit_params[_layer_number]

                # searching window estimation parameters for "func"
                # fit_param = [-1.33664766, 6.27050496], q > 0
                # fit_param = [-1.29316938  6.11621531], q < 0

            for track, hit in zip(tracks,hits):
                x_predicted = track[0] * (layer_z_position - track[4]) + track[1]
                x_true = hit[0]
                p = np.abs(track[5])

                x_correction = 0.
                if _corrected:
                    x_correction = func(p, fit_param[0], fit_param[1])

                if _save_root:
                    out_ntuple.Fill(p, x_true, x_predicted, x_correction, track[6])

                if x_correction + track[6] * (x_true - x_predicted) > 100 and\
                   x_correction + track[6] * (x_true - x_predicted) < -150: continue

                if track[6] > 0:
                    p_plus += (p,)
                    if _corrected:
                        dx_plus += (np.abs(x_correction + x_true - x_predicted),)
                    else:
                        dx_plus += (x_predicted - x_true,)
                else:
                    p_minus += (p,)
                    if _corrected:
                        dx_minus += (np.abs(x_correction - x_true + x_predicted),)
                    else:
                        dx_minus += (x_true - x_predicted,)

    if _save_root:
        output_file.Write()
        output_file.Close()

    plt.figure(figsize=(6,4))
    # plt.gca().set(title='dx(p), q > 0', xlabel='p [MeV/c]', ylabel='dx [mm]')
    plt.gca().set(title='', xlabel='p [MeV/c]', ylabel='dx [mm]')
    plt.scatter(p_plus, dx_plus, marker = '.')

    if _fit:
        xopt, xcov = optimize.curve_fit(func, p_plus, dx_plus)
        plt.plot(np.sort(p_plus), func(np.sort(p_plus), *xopt), 'r--', label='fit: k=%5.3f, p0=%5.3f' % tuple(xopt))
        print('dx_plus, func fit, red: ')
        print(xopt)
        print(xcov)
        print('\n')
        plt.savefig('../output/dx_p_plus.png')
        plt.savefig('../output/dx_p_plus.pdf')

        # xopt, xcov = optimize.curve_fit(func1, p_plus, dx_plus)
        # plt.plot(np.sort(p_plus), func1(np.sort(p_plus), *xopt), 'y--', label='fit: a=%5.3f, b=%5.3f' % tuple(xopt))
        # print('dx_plus, func1 fit, yellow: ')
        # print(xopt)
        # print(xcov)
        # print('\n')

    plt.figure(figsize=(6,4))
    # plt.gca().set(title='dx(p), q < 0', xlabel='p [MeV/c]', ylabel='dx [mm]')
    plt.gca().set(title='', xlabel='p [MeV/c]', ylabel='dx [mm]')
    plt.scatter(p_minus, dx_minus, marker = '.')

    if _fit:
        xopt, xcov = optimize.curve_fit(func, p_minus, dx_minus)
        plt.plot(np.sort(p_minus), func(np.sort(p_minus), *xopt), 'r--', label='fit: k=%5.3f, p0=%5.3f' % tuple(xopt))
        print('dx_minus, func fit, red: ')
        print(xopt)
        print(xcov)
        print('\n')
        plt.savefig('../output/dx_p_minus.png')
        plt.savefig('../output/dx_p_minus.pdf')

        # xopt, xcov = optimize.curve_fit(func1, p_minus, dx_minus)
        # plt.plot(np.sort(p_minus), func1(np.sort(p_minus), *xopt), 'y--', label='fit: a=%5.3f, b=%5.3f' % tuple(xopt))
        # print('dx_minus, func1 fit, yellow: ')
        # print(xopt)
        # print(xcov)
        # print('\n')

    plt.show()

def dy_p_tot(_layer_number):
    p = ()
    dy = ()

    # for run_number in [2500]:
    #     for event_number in [3]:
    for run_number in [2500, 2501, 2503, 2504, 2505]:
        for event_number in range(1, 51, 1):
            # open preprocessed data file
            process_file = up.open("mcparticle_data/" + str(run_number) + "_" + str(event_number) + "_FThits.root")

            #                                                       0  1  2  3  4 5 6   7
            # TNtuple("line", "upstream track's line parameters", "kx:x0:ky:y0:z0:p:q:track")
            tracks = np.asarray(process_file["line"].arrays(["*"], outputtype=tuple)).T

            layers = ["s0l0", "s0l3", "s1l0", "s1l3", "s2l0", "s2l3"]
            hits = np.asarray(process_file[layers[_layer_number]].arrays(["*"], outputtype=tuple)).T

            layer_z_positions = [7826.13, 8035.95,\
                                 8508.14, 8717.93,\
                                 9193.17, 9402.96]
            layer_z_position = layer_z_positions[_layer_number]

            for track, hit in zip(tracks,hits):
                y_predicted = track[2] * (layer_z_position - track[4]) + track[3]
                y_true = hit[1]
                p += (np.abs(track[5]),)
                dy += (np.abs(y_predicted - y_true),)


    plt.figure(figsize=(6,4))
    plt.gca().set(title='dy(p)', xlabel='p [MeV/c]', ylabel='dy [mm]')
    plt.scatter(p, dy, marker = '.')

    yopt, ycov = optimize.curve_fit(func, p, dy)
    plt.plot(np.sort(p), func(np.sort(p), *yopt), 'r--', label='fit')
    print('dy, func fit, red: ')
    print(yopt)
    print(ycov)
    print('\n')

    plt.show()

def dy_p(_layer_number, _save_root = 0, _fit = 0, _corrected = 1):
    if _save_root:
        output_file = TFile.Open("mcparticle_data/" + str(_layer_number) + "_dy_p.root", "RECREATE")
        out_ntuple = TNtuple("tuple", "tuple", "p:y_true:y_predicted:q:kx:ky")

    p_plus  = ()
    dy_plus = ()
    p_minus  = ()
    dy_minus = ()

    # for run_number in [2500]:
    #     for event_number in [3]:
    for run_number in [2500, 2501, 2503, 2504, 2505]:
        for event_number in range(1, 51, 1):
            # open preprocessed data file
            process_file = up.open("mcparticle_data/" + str(run_number) + "_" + str(event_number) + "_FThits.root")

            #                                                       0  1  2  3  4 5 6   7
            # TNtuple("line", "upstream track's line parameters", "kx:x0:ky:y0:z0:p:q:track")
            tracks = np.asarray(process_file["line"].arrays(["*"], outputtype=tuple)).T

            layers = ["s0l0", "s0l3", "s1l0", "s1l3", "s2l0", "s2l3"]
            hits = np.asarray(process_file[layers[_layer_number]].arrays(["*"], outputtype=tuple)).T

            layer_z_positions = [7826.13, 8035.95,\
                                 8508.14, 8717.93,\
                                 9193.17, 9402.96]
            layer_z_position = layer_z_positions[_layer_number]

            if _corrected:
                fit_param = [-1.03523872, 4.62380112]

                # searching window estimation parameters for "func"
                # fit_param = [-1.03523872, 4.62380112]

            for track, hit in zip(tracks,hits):
                y_predicted = track[2] * (layer_z_position - track[4]) + track[3]
                y_true = hit[1]
                p = np.abs(track[5])

                if _save_root:
                    out_ntuple.Fill(p, y_true, y_predicted, track[6], track[0], track[2])

                if np.abs(y_predicted - y_true) > 50: continue

                if _corrected:
                    y_correction = func(p, fit_param[0], fit_param[1])

                if track[2] > 0:
                    p_plus += (p,)
                    if _corrected:
                        dy_plus += (y_correction + y_true - y_predicted,)
                    else:
                        dy_plus += (np.abs(y_predicted - y_true),)
                else:
                    p_minus += (p,)
                    if _corrected:
                        dy_minus += (y_correction - y_true + y_predicted,)
                    else:
                        dy_minus += (np.abs(y_true - y_predicted),)

    if _save_root:
        output_file.Write()
        output_file.Close()

    plt.figure(figsize=(6,4))
    plt.gca().set(title='dy(p), ky > 0', xlabel='p [MeV/c]', ylabel='dy [mm]')
    plt.scatter(p_plus, dy_plus, marker = '.')

    if _fit:
        yopt, ycov = optimize.curve_fit(func, p_plus, dy_plus)
        plt.plot(np.sort(p_plus), func(np.sort(p_plus), *yopt), 'r--', label='fit')
        print('dy_plus, func fit, red: ')
        print(yopt)
        print(ycov)
        print('\n')
        plt.savefig('../output/dy_p_plus.png')

    plt.figure(figsize=(6,4))
    plt.gca().set(title='dy(p), ky < 0', xlabel='p [MeV/c]', ylabel='dy [mm]')
    plt.scatter(p_minus, dy_minus, marker = '.')

    if _fit:
        yopt, ycov = optimize.curve_fit(func, p_minus, dy_minus)
        plt.plot(np.sort(p_minus), func(np.sort(p_minus), *yopt), 'r--', label='fit')
        print('dy_minus, func fit, red: ')
        print(yopt)
        print(ycov)
        print('\n')
        plt.savefig('../output/dy_p_minus.png')

    plt.show()

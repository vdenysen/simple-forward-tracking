import numpy as np

def smearing_p(_p):
    """
    Smearing of the momentum of the particle
    due to the claimed 20 % of the VeLo-UT momentum estimation

    Args: p = np.array([float]), momentum of the particle
    """
    return np.random.normal(loc = _p, scale = 0.2 * np.abs(_p)) # 20 %

def smearing_velo_xy(_pos):
    """
    Smearing of the x coordinate of the hit
    due to the claimed pixel size of the VeLo pixel_size = 55 x 55 um2

    Args: _pos = np.array([float]), coordinate of the hit
    """
    return np.random.normal(loc = _pos, scale = (0.055 / np.sqrt(12))) # 55 um

def smearing_ut_x(_x):
    """
    Smearing of the x coordinate of the hit
    assuming the same pixel size as for MT x_pixel_size = 100 um

    Args: _x = np.array([float]), coordinate of the hit
    """
    return np.random.normal(loc = _x, scale = (0.1 / np.sqrt(12))) # 100 um

def smearing_ut_y(_y):
    """
    Smearing of the x coordinate of the hit
    assuming the same pixel size as for MT y_pixel_size = 300 um

    Args: _y = np.array([float]), coordinate of the hit
    """
    return np.random.normal(loc = _y, scale = (0.3 / np.sqrt(12))) # 300 um

def smearing_mt_m_x(_x):
    """
    Smearing of the x coordinate of the hit
    due to the claimed pixel size x_pixel_size = 100 um

    Args: _x = np.array([float]), coordinate of the hit
    """
    return np.random.normal(loc = _x, scale = (0.1 / np.sqrt(12))) # 100 um

def smearing_mt_m_y(_y):
    """
    Smearing of the y coordinate of the hit
    due to the claimed pixel size y_pixel_size = 300 um

    Args: _y = np.array([float]), coordinate of the hit
    """
    return np.random.normal(loc = _y, scale = (0.3 / np.sqrt(12))) # 300 um

def smearing_mt_o_x(_x):
    """
    Smearing of the x coordinate of the hit
    according to the fibre size x_fibre_size = 250 um

    Args: _x = np.array([float]), coordinate of the hit
    """
    return np.random.normal(loc = _x, scale = (0.25 / np.sqrt(12))) # 250 um

def smearing_mt_o_y(_y):
    """
    Smearing of the y coordinate of the hit
    according to the fibre size x_fibre_size = 250 um and tilt = 5 deg

    Args: _y = np.array([float]), coordinate of the hit
    """
    return np.random.normal(loc = _y, scale = (0.25 / np.sin(5 * np.pi / 180))) # 5 deg

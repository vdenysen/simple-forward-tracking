import numpy as np
from scipy import spatial

def is_inside(_x, _y):
    """
    Checking whether particle pathing through the middle part of the MT or not

    Args:
        _x = np.array([float]), x coordinate of the hit
        _y = np.array([float]), y coordinate of the hit
    """
    x_block = 530
    y_block = 200

    if abs(_x) > 4.01 * x_block and abs(_y) > 2.51 * y_block:
        return False

    x_blocks = np.array([ 0.5,  1.5,  1.5,  2.5,  2.5,  3.5,  3.5,  2.5,  1.5,  1.5,  0.5, -0.5, -1.5, -1.5, -2.5, -2.5, -3.5, -3.5, -2.5, -1.5, -1.5, -0.5])
    y_blocks = np.array([-2.0, -1.5, -0.5, -1.0,  0.0, -0.5,  0.5,  1.0,  0.5,  1.5,  2.0, -2.0, -1.5, -0.5, -1.0,  0.0, -0.5,  0.5,  1.0,  0.5,  1.5,  2.0])

    for x, y in zip(x_blocks, y_blocks):
        if abs(_x - x * x_block) <= x_block / 2. and abs(_y - y * y_block) <= y_block / 2.:
            return True

    return False

def best_hit(_x_prediction, _y_prediction, _y_direction, _y_previous, _x_direction, _x_previous, _layer, hits):
    """
    Hit finding in the layer using "the best candidate" approach

    Args:
        prediction = [float], hit coordinate prediction in the layer
        direction  = [int], slope sign
        previous   = [float],  hit y coordinate in previous layer
        layer      = [int], number of the layer we are looking for hit in
        hits       = [], array of all the hits in the specific MT layer
    """
    prediction = np.array([_x_prediction, _y_prediction])
    if _y_direction > 0.:
        hits_tmp = hits[_layer][hits[_layer][:,1] > _y_previous]
    else:
        hits_tmp = hits[_layer][hits[_layer][:,1] < _y_previous]

    if _x_direction > 0.:
        hits_tmp = hits_tmp[hits_tmp[:,0] > _x_previous]
    else:
        hits_tmp = hits_tmp[hits_tmp[:,0] < _x_previous]

    if len(hits_tmp) > 0.:
        distance, index = spatial.KDTree(hits_tmp[:,:2]).query(prediction)
    else:
        return None

    return hits_tmp[index], distance

def reduced_chi2_x(z, x, f, *pars):
    return (((f(z, *pars) - x) / (0.1 / np.sqrt(12)))**2).sum(), len(x) - len(pars)

def reduced_chi2_y(z, y, f, *pars):
    return (((f(z, *pars) - y) / (0.3 / np.sqrt(12)))**2).sum(), len(y) - len(pars)
